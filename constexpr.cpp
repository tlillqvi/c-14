#include <iostream>

constexpr int array_len()
{
  return 10;
}

int main(int argc, char *argv[])
{
  int arr[array_len()];
  if( sizeof(arr)/sizeof(int) == array_len() )
  {
    std::cout << "len: " << sizeof(arr)/sizeof(int) << std::endl;
  }
}
