#include <iostream>
#include <mutex>
#include <functional>
#include <atomic>

using namespace std;

void run_locked(mutex& m, auto f)
{
  lock_guard<mutex> scopelock(m);
  f();
}

int main(int argc, char* argv[])
{
  mutex m;
  auto f = [](){ cout << "Hello world" << endl; };
  run_locked( m, f );
  atomic<int> test{0};
  test++;
}
