CXXFLAGS=-g -Wall -std=c++14
CXX=g++


.PHONY: all


all: lock_guard constexpr

lock_guard: lock_guard.o
	$(CXX) $< -o $@

constexpr: constexpr.o
	$(CXX) $< -o $@

